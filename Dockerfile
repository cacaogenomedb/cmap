FROM httpd:2.4.46

# * Filesys::DfPortable is unavailable for Debian buster; substitute Filesys::Df
# * Assuming DBD::SQLite database
#   - TODO: CMap should include a small script tool to create/import DB without need for sqlite3
#   - Database should be loaded at container start time, rather than baked into the image
# * xz for Cacao Genome DB cmap export file

RUN apt update && \
    apt install -y \
                   libapache-htpasswd-perl \
                   libbit-vector-perl \
                   libcache-cache-perl \
                   libcgi-pm-perl \
                   libcgi-session-perl \
                   libclass-base-perl \
                   libclone-perl \
                   libconfig-general-perl \
                   libdata-pageset-perl \
                   libdbd-sqlite3-perl \
                   libfilesys-df-perl \
                   libfont-freetype-perl \
                   libgd-perl \
                   libgd-svg-perl \
                   libio-tee-perl \
                   libparams-validate-perl \
                   libregexp-common-perl \
                   libtemplate-perl \
                   libtemplate-plugin-comma-perl \
                   libtext-recordparser-perl \
                   libtime-parsedate-perl \
                   libxml-simple-perl \
                   perl-modules \
                   sqlite3 \
                   wget \
                   xz-utils && \
    cd /srv/ && \
    wget -O - https://github.com/GMOD/cmap/archive/0f007bf.tar.gz | tar -xzvf - && \
    mv cmap-* cmap && \
    sed -i.bak -e 's/Filesys::DfPortable/Filesys::Df/' -e 's/dfportable/df/' /srv/cmap/lib/Bio/GMOD/CMap.pm && \
    ln -s /tmp/cmap /srv/cmap/htdocs/tmp && \
    apt autoremove -y wget && \
    rm -rf /var/lib/apt/lists/*

COPY ./httpd-cmap.conf /usr/local/apache2/conf/

ENV CMAP_ROOT="/srv/cmap/"
ENV PATH="/srv/cmap/bin:${PATH}"
ENV PERL5LIB="/srv/cmap/lib/"

WORKDIR /srv/cmap/

COPY ./cmap.conf conf/

COPY ./cmap_export.xml.xz data/

RUN sqlite3 data/cacao_cmap_public_live.db < sql/cmap.create.sqlite && \
    unxz data/cmap_export.xml.xz && \
    cmap_admin.pl -d CACAO --action import_object_data --no-log data/cmap_export.xml && \
    rm -f data/cmap_export.xml && \
    sqlite3 data/cacao_cmap_public_live.db ANALYZE

EXPOSE 80

ENTRYPOINT httpd -DFOREGROUND -C 'LoadModule cgid_module modules/mod_cgid.so' -c 'Include /usr/local/apache2/conf/httpd-cmap.conf'
